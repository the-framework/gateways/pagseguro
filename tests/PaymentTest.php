<?php namespace Tests\PagSeguro;

use Framework\PagSeguro\Config;
use Framework\PagSeguro\Payment;
use Framework\Shop\Product;
use PHPUnit\Framework\TestCase;

class PaymentTest extends TestCase
{
	protected Payment $payment;

	public function setup() : void
	{
		$config = new Config();
		$config->email = 'natanfelles@gmail.com';
		$config->token = 'C30774339ADE4141A38A41C3EBF5AA5F';
		$config->redirectURL = 'http://localhost/checkout';
		$config->notificationURL = 'http://localhost/notification';
		$config->logActive = false;
		$this->payment = new Payment($config);
	}

	protected function setDefaults()
	{
		$this->payment->setCustomer('John Doe', 'v78316124706023444707@sandbox.pagseguro.com.br');

		$item1 = new Product();
		$item1->id = 'ABC123';
		$item1->title = 'Monitor LG';
		$item1->price = '400.00';

		$item2 = new Product();
		$item2->id = 'XYZ';
		$item2->title = 'Teclado Positivo';
		$item2->price = 40.00;
		$item2->quantity = 2;

		$this->payment->setOrder([
			$item1,
			$item2,
		], 'ABC123');

		$this->payment->setShipping(20, $this->payment::SHIP_TYPE_PAC);
		$this->payment->setShippingAddress(
			'Av. Brig. Faria Lima',
			1384,
			'Jardim Paulistano',
			'01452002',
			'São Paulo',
			'SP',
			'BRA',
			'apto. 114'
		);
		$this->payment->setCustomerPhone(51, 999887766);
		$this->payment->setCustomerCPF(32387395069);
	}

	public function testGetLink()
	{
		$this->setDefaults();
		$link = $this->payment->getLink();
		$this->assertStringStartsWith(
			'https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html?code=',
			$link
		);
	}

	public function testGetCode()
	{
		$this->setDefaults();
		$code = $this->payment->getCode();
		$this->assertEquals(32, \strlen($code));
	}
}
