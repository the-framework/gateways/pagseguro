# Framework PagSeguro Gateway Package

- [Homepage](https://gitlab.com/the-framework/gateways/pagseguro)
- [API Documentation](https://the-framework.gitlab.io/gateways/pagseguro/docs/)

[![Build](https://gitlab.com/the-framework/gateways/pagseguro/badges/master/pipeline.svg)](https://gitlab.com/the-framework/gateways/pagseguro/-/jobs)
[![Coverage](https://gitlab.com/the-framework/gateways/pagseguro/badges/master/coverage.svg?job=test:php)](https://the-framework.gitlab.io/gateways/pagseguro/coverage/)
