<?php namespace Framework\PagSeguro;

use PagSeguro\Configuration\Configure;
use PagSeguro\Helpers\Xhr;
use PagSeguro\Library;
use PagSeguro\Services\Transactions\Notification as PagSeguroNotification;

class Notification
{
	protected bool $logActive;
	protected string $logNotificationPath;

	public function __construct(Config $config)
	{
		Library::initialize();
		Configure::setEnvironment($config->environment);
		Configure::setAccountCredentials($config->email, $config->token);
		Configure::setCharset($config->charset);
		Configure::setLog($config->logActive, $config->logPath);
		$this->logActive = $config->logActive;
		$this->logNotificationPath = $config->logNotificationPath;
	}

	public function register()
	{
		if ( ! Xhr::hasPost()) {
			throw new \InvalidArgumentException($_POST);
		}
		$response = PagSeguroNotification::check(
			Configure::getAccountCredentials()
		);
		if ($this->logActive) {
			\file_put_contents($this->logNotificationPath, \print_r($response, true));
		}
		return $response;
	}
}
