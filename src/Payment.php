<?php namespace Framework\PagSeguro;

use Framework\Shop\PaymentInterface;
use Framework\Shop\Product;
use PagSeguro\Configuration\Configure;
use PagSeguro\Domains\Requests\Payment as PagSeguroPayment;
use PagSeguro\Library;

class Payment implements PaymentInterface
{
	public const SHIP_TYPE_PAC = \PagSeguro\Enum\Shipping\Type::PAC;
	public const SHIP_TYPE_SEDEX = \PagSeguro\Enum\Shipping\Type::SEDEX;
	public const SHIP_TYPE_NOT_SPECIFIED = \PagSeguro\Enum\Shipping\Type::NOT_SPECIFIED;
	protected PagSeguroPayment $payment;

	public function __construct(Config $config)
	{
		Library::initialize();
		Configure::setEnvironment($config->environment);
		Configure::setAccountCredentials($config->email, $config->token);
		Configure::setCharset($config->charset);
		Configure::setLog($config->logActive, $config->logPath);
		$this->payment = new PagSeguroPayment();
		$this->payment->setCurrency($config->currency);
		$this->payment->setRedirectUrl($config->redirectURL);
		$this->payment->setNotificationUrl($config->notificationURL);
	}

	public function setCustomer(string $name, string $email)
	{
		$this->payment->setSender()->setName($name);
		$this->payment->setSender()->setEmail($email);
		return $this;
	}

	public function setCustomerPhone($areaCode, $number)
	{
		$this->payment->setSender()->setPhone()->withParameters(
			$areaCode,
			$number
		);
		return $this;
	}

	public function setCustomerCPF($cpf)
	{
		$this->payment->setSender()->setDocument()->withParameters(
			'CPF',
			$cpf
		);
		return $this;
	}

	/**
	 * @param float $cost Shipping price
	 * @param int   $type One of the SHIP_TYPE_ constants
	 *
	 * @return $this
	 */
	public function setShipping(float $cost, int $type)
	{
		$this->payment->setShipping()->setCost()->withParameters($cost);
		$this->payment->setShipping()->setType()->withParameters($type);
		return $this;
	}

	/**
	 * @param string      $street
	 * @param int         $number
	 * @param string      $district
	 * @param string      $postalCode
	 * @param string      $city
	 * @param string      $state      State code with 2 chars
	 * @param string      $country    Country code with 3 chars
	 * @param string|null $complement
	 *
	 * @return $this
	 */
	public function setShippingAddress(
		string $street,
		int $number,
		string $district,
		string $postalCode,
		string $city,
		string $state,
		string $country = 'BRA',
		string $complement = null
	) {
		$this->payment->setShipping()->setAddress()->withParameters(
			$street,
			$number,
			$district,
			$postalCode,
			$city,
			$state,
			$country,
			$complement = null
		);
		return $this;
	}

	/**
	 * @param array|Product[] $products
	 * @param string|null     $invoice_number
	 *
	 * @return $this
	 */
	public function setOrder(array $products, string $invoice_number = null)
	{
		if ($invoice_number !== null) {
			$this->payment->setReference($invoice_number);
		}

		foreach ($products as $product) {
			$this->payment->addItems()->withParameters(
				$product->id,
				$product->title,
				$product->quantity,
				$product->price
			);
		}
		return $this;
	}

	public function getLink() : string
	{
		$credentials = Configure::getAccountCredentials();
		return $this->payment->register($credentials);
	}

	public function getCode() : string
	{
		$credentials = Configure::getAccountCredentials();
		return $this->payment->register($credentials, true)->getCode();
	}
}
