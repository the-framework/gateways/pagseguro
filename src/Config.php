<?php namespace Framework\PagSeguro;

class Config
{
	/**
	 * @var string production or sandbox
	 */
	public string $environment = 'sandbox';
	/**
	 * @var string Seller email
	 */
	public string $email;
	/**
	 * @see https://sandbox.pagseguro.uol.com.br/vendedor/configuracoes.html
	 * @see https://pagseguro.uol.com.br/preferencias/integracoes.jhtml
	 *
	 * @var string Seller Token
	 */
	public string $token;
	/**
	 * @var string Accepted charset. UTF-8 or ISO-8859-1
	 */
	public string $charset = 'UTF-8';
	/**
	 * @var string Accepted currency
	 */
	public string $currency = 'BRL';
	/**
	 * @var string URL to redirect back after payment
	 */
	public string $redirectURL;
	/**
	 * @var string Notification URL for payment status changes
	 */
	public string $notificationURL;
	/**
	 * @var bool Log status
	 */
	public bool $logActive = true;
	/**
	 * @var string Payment log path
	 */
	public string $logPath = __DIR__ . '/../logs/pagseguro.log';
	/**
	 * @var string Notification log path
	 */
	public string $logNotificationPath = __DIR__ . '/../logs/pagseguro-notification.log';
}
